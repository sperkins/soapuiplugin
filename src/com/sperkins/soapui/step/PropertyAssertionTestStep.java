package com.sperkins.soapui.step;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import junit.framework.AssertionFailedError;

import org.junit.Assert;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepWithProperties;
import com.eviware.soapui.model.propertyexpansion.PropertyExpander;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansion;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionContainer;
import com.eviware.soapui.model.propertyexpansion.PropertyExpansionUtils;
import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.UISupport;
import com.eviware.soapui.support.xml.XmlObjectConfigurationBuilder;
import com.eviware.soapui.support.xml.XmlObjectConfigurationReader;

public class PropertyAssertionTestStep extends WsdlTestStepWithProperties implements PropertyExpansionContainer {

	public static final String MESSAGE_PROPERTY = "message";
	public static final String EXPECTED_VALUE_PROPERTY = "expected";
	public static final String EXPECTED_VALUE_TYPE_PROPERTY = "expectedType";
	public static final String ACTUAL_VALUE_PROPERTY = "actual";
	public static final String ACTUAL_VALUE_TYPE_PROPERTY = "actualType";
	public static final String COMPARISON_TYPE_PROPERTY = "comparison";

	private String message;
	private String expected;
	private ValueType expectedType;
	private String actual;
	private ValueType actualType;
	private PropertyAssertionType comparison;

	public PropertyAssertionTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
		super(testCase, config, true, forLoadTest);
		if (!forLoadTest) {
			setIcon(UISupport.createImageIcon("icon.png"));
		}
		// Load properties for this test step
		readConfig(config);
	}

	@Override
	public TestStepResult run(TestCaseRunner testRunner, TestCaseRunContext runContext) {
		WsdlTestStepResult result = new WsdlTestStepResult(this);
		result.startTimer();

		SoapUI.log.info("Message: " + getMessage());
		SoapUI.log.info("Expected value: " + getExpected());
		SoapUI.log.info("Actual value: " + getActual());
		SoapUI.log.info("Comparison type: " + getComparison());

		String expandedMessage = PropertyExpander.expandProperties(runContext, getMessage());
		String expandedExpected = PropertyExpander.expandProperties(runContext, getExpected());
		String expandedActual = PropertyExpander.expandProperties(runContext, getActual());

		SoapUI.log.info("Expanded message: " + expandedMessage);
		SoapUI.log.info("Expanded expected: " + expandedExpected);
		SoapUI.log.info("Expanded actual: " + expandedActual);

		try {
			if (null == comparison) {
				throw new IllegalArgumentException("PropertyAssertionTestStep: No comparison type selected!");
			}

			// Try to find the comparison's equivalent method on the org.junit.Assert object
			Class<Assert> assertClass = Assert.class;
			// We only allow up to two parameters for simplicity's sake

			boolean useExpectedParam = false;
			List<Class> parameterClasses = new ArrayList<Class>();
			if (!ValueType.NOT_USED.equals(expectedType)) {
				parameterClasses.add(expectedType.getClazz());
				useExpectedParam = true;
			}

			parameterClasses.add(actualType.getClazz());

			Method assertMethod = assertClass.getMethod(comparison.getMethod(), parameterClasses.toArray(new Class[0]));
			if (useExpectedParam) {
				assertMethod.invoke(expectedType.getClazz().cast(expandedExpected), actualType.getClazz().cast(expandedActual));
			} else {
				assertMethod.invoke(actualType.getClazz().cast(expandedActual));
			}

			result.stopTimer();
			result.setStatus(TestStepResult.TestStepStatus.OK);

		} catch (IllegalArgumentException e) {
			logException(testRunner, e);
		} catch (SecurityException e) {
			logException(testRunner, e);
		} catch (NoSuchMethodException e) {
			logException(testRunner, e);
		} catch (IllegalAccessException e) {
			logException(testRunner, e);
		} catch (InvocationTargetException e) {
			logException(testRunner, e);
		} catch (AssertionFailedError e) {
			logException(testRunner, e);
		}
		return result;
	}

	/**
	 * Loads all persisted properties for this test step
	 * 
	 * @param config
	 */
	private void readConfig(TestStepConfig config) {
		XmlObjectConfigurationReader reader = new XmlObjectConfigurationReader(config.getConfig());
		this.message = reader.readString(MESSAGE_PROPERTY, "");
		this.expected = reader.readString(EXPECTED_VALUE_PROPERTY, null);
		this.actual = reader.readString(ACTUAL_VALUE_PROPERTY, null);

		// Read out and convert the expected value type
		String valueTypeString = reader.readString(EXPECTED_VALUE_TYPE_PROPERTY, null);
		if (null != valueTypeString) {
			expectedType = ValueType.valueOf(valueTypeString);
		} else {
			expectedType = ValueType.NOT_USED;
		}

		// Read out and convert the actual value type
		valueTypeString = reader.readString(ACTUAL_VALUE_TYPE_PROPERTY, null);
		if (null != valueTypeString) {
			actualType = ValueType.valueOf(valueTypeString);
		} else {
			actualType = ValueType.NOT_USED;
		}

		// Read out and convert the comparison type
		String comparisonTypeName = reader.readString(COMPARISON_TYPE_PROPERTY, null);
		if (null != comparisonTypeName) {
			this.comparison = PropertyAssertionType.valueOf(comparisonTypeName);
		}
	}

	/**
	 * Persists all properties for this test step
	 */
	private void updateConfig() {
		XmlObjectConfigurationBuilder builder = new XmlObjectConfigurationBuilder();
		builder.add(EXPECTED_VALUE_PROPERTY, this.expected);
		builder.add(ACTUAL_VALUE_PROPERTY, this.actual);
		if (null != this.comparison) {
			builder.add(COMPARISON_TYPE_PROPERTY, this.comparison.name());
		} else {
			// Remove property if user is not setting it right now
			builder.add(COMPARISON_TYPE_PROPERTY, (String) null);
		}
		getConfig().setConfig(builder.finish());
	}

	@Override
	public PropertyExpansion[] getPropertyExpansions() {
		List result = new ArrayList();
		result.addAll(PropertyExpansionUtils.extractPropertyExpansions(this, this, MESSAGE_PROPERTY));
		result.addAll(PropertyExpansionUtils.extractPropertyExpansions(this, this, EXPECTED_VALUE_PROPERTY));
		result.addAll(PropertyExpansionUtils.extractPropertyExpansions(this, this, ACTUAL_VALUE_PROPERTY));
		return (PropertyExpansion[]) result.toArray(new PropertyExpansion[result.size()]);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		String old = this.message;
		updateConfig();
		this.message = message;
		notifyPropertyChanged(MESSAGE_PROPERTY, old, message);
	}

	public String getExpected() {
		return expected;
	}

	public void setExpected(String expected) {
		String old = this.expected;
		updateConfig();
		this.expected = expected;
		notifyPropertyChanged(EXPECTED_VALUE_PROPERTY, old, expected);
	}

	public String getActual() {
		return actual;
	}

	public void setActual(String actual) {
		String old = this.actual;
		updateConfig();
		this.actual = actual;
		notifyPropertyChanged(ACTUAL_VALUE_PROPERTY, old, actual);
	}

	public ValueType getExpectedType() {
		return expectedType;
	}

	public void setExpectedType(ValueType expectedType) {
		ValueType old = this.expectedType;
		updateConfig();
		this.expectedType = expectedType;
		notifyPropertyChanged(EXPECTED_VALUE_TYPE_PROPERTY, old, expectedType);
	}

	public ValueType getActualType() {
		return actualType;
	}

	public void setActualType(ValueType actualType) {
		ValueType old = this.actualType;
		updateConfig();
		this.actualType = actualType;
		notifyPropertyChanged(ACTUAL_VALUE_TYPE_PROPERTY, old, actualType);
	}

	public PropertyAssertionType getComparison() {
		return comparison;
	}

	public void setComparison(PropertyAssertionType comparison) {
		PropertyAssertionType old = this.comparison;
		updateConfig();
		this.comparison = comparison;
		notifyPropertyChanged(COMPARISON_TYPE_PROPERTY, old, comparison);
	}

	private void logException(TestCaseRunner testRunner, Throwable e) {
		SoapUI.log.error(e);
		testRunner.fail(e.getMessage());
	}

	private void fail(TestCaseRunner runner, String message) {
		SoapUI.log.error(message);
		runner.fail(message);
	}
}
