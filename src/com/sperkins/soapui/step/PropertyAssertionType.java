package com.sperkins.soapui.step;

public enum PropertyAssertionType {
	EQUALS("Equals", "assertEquals"), NULL("Null", "assertNull"), NOT_NULL("Not null", "assertNotNull");

	private final String description;
	private final String method;

	private PropertyAssertionType(String description, String method) {
		this.description = description;
		this.method = method;
	}

	public String getDescription() {
		return description;
	}

	public String getMethod() {
		return method;
	}

	@Override
	public String toString() {
		return getDescription();
	}
}
