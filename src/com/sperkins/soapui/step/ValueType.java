package com.sperkins.soapui.step;

public enum ValueType {
	NOT_USED(null, null),
	STRING("String", String.class),
	LONG("Long", Long.class),
	DOUBLE("Double", Double.class),
	OBJECT("Object", Object.class);

	private final String description;
	private final Class clazz;

	private ValueType(String description, Class clazz) {
		this.description = description;
		this.clazz = clazz;
	}

	public String getDescription() {
		return description;
	}

	public Class getClazz() {
		return clazz;
	}

	@Override
	public String toString() {
		return getDescription();
	}
}
