package com.sperkins.soapui.step.factory;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.impl.wsdl.teststeps.registry.WsdlTestStepFactory;
import com.sperkins.soapui.step.PropertyAssertionTestStep;

public class PropertyAssertionTestStepFactory extends WsdlTestStepFactory {

	private static final String PROPERTY_ASSERTION_TEST_STEP_ID = "propertyassertion";

	public PropertyAssertionTestStepFactory() {
		super(PROPERTY_ASSERTION_TEST_STEP_ID, "Property Assertion", "Property assertion test step", null);
		SoapUI.getFactoryRegistry().addFactory(PropertyAssertionTestStepFactory.class, this);
	}

	@Override
	public WsdlTestStep buildTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
		return new PropertyAssertionTestStep(testCase, config, forLoadTest);
	}

	@Override
	public TestStepConfig createNewTestStep(WsdlTestCase testCase, String name) {
		TestStepConfig testStepConfig = TestStepConfig.Factory.newInstance();
		testStepConfig.setType(PROPERTY_ASSERTION_TEST_STEP_ID);
		testStepConfig.setName(name);

		return testStepConfig;
	}

	@Override
	public boolean canCreate() {
		return true;
	}
}
