package com.sperkins.soapui.step.ui;

import com.eviware.soapui.ui.support.ModelItemDesktopPanel;
import com.sperkins.soapui.step.PropertyAssertionTestStep;

public class PropertyAssertionTestStepDesktopPanel extends ModelItemDesktopPanel<PropertyAssertionTestStep> {

	private static final long serialVersionUID = 2456754645016182446L;

	public PropertyAssertionTestStepDesktopPanel(PropertyAssertionTestStep modelItem) {
		super(modelItem);
	}
}