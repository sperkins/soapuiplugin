package com.sperkins.soapui.step.ui.factory;

import javax.swing.JPanel;

import com.eviware.soapui.impl.EmptyPanelBuilder;
import com.eviware.soapui.model.PanelBuilder;
import com.eviware.soapui.model.util.PanelBuilderFactory;
import com.eviware.soapui.support.components.JPropertiesTable;
import com.eviware.soapui.ui.desktop.DesktopPanel;
import com.sperkins.soapui.step.PropertyAssertionTestStep;
import com.sperkins.soapui.step.PropertyAssertionType;
import com.sperkins.soapui.step.ValueType;
import com.sperkins.soapui.step.ui.PropertyAssertionTestStepDesktopPanel;

public class PropertyAssertionTestStepPanelBuilderFactory implements PanelBuilderFactory<PropertyAssertionTestStep> {

	@Override
	public PanelBuilder<PropertyAssertionTestStep> createPanelBuilder() {
		return new PropertyAssertionTestStepPanelBuilder();
	}

	@Override
	public Class<PropertyAssertionTestStep> getTargetModelItem() {
		return PropertyAssertionTestStep.class;
	}

	public static class PropertyAssertionTestStepPanelBuilder extends EmptyPanelBuilder<PropertyAssertionTestStep> {

		@Override
		public DesktopPanel buildDesktopPanel(PropertyAssertionTestStep modelItem) {
			return new PropertyAssertionTestStepDesktopPanel(modelItem);
		}

		@Override
		public boolean hasDesktopPanel() {
			return true;
		}

		@Override
		public JPanel buildOverviewPanel(PropertyAssertionTestStep modelItem) {
			JPropertiesTable<PropertyAssertionTestStep> table = new JPropertiesTable<PropertyAssertionTestStep>("Assertion Properties");
			table.addProperty("Expected value", PropertyAssertionTestStep.EXPECTED_VALUE_PROPERTY, true);
			table.addProperty("Expected value type", PropertyAssertionTestStep.EXPECTED_VALUE_TYPE_PROPERTY, ValueType.values());
			table.addProperty("Actual value", PropertyAssertionTestStep.ACTUAL_VALUE_PROPERTY, true);
			table.addProperty("Actual value type", PropertyAssertionTestStep.ACTUAL_VALUE_TYPE_PROPERTY, ValueType.values());
			table.addProperty("Comparision type", PropertyAssertionTestStep.COMPARISON_TYPE_PROPERTY, PropertyAssertionType.values());
			table.setPropertyObject(modelItem);
			return table;
		}

		@Override
		public boolean hasOverviewPanel() {
			return true;
		}
	}
}